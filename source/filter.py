from pymongo import MongoClient

ORDER_ASCENDING = 1
ORDER_DESCENDING = -1

# ------------------------ Event level filters ------------------------

# Filters the given collection based on the passed attribute.
# Only events in which the attribute corrsponds to one of the given values are returned.
def event_attribute_one_of(attribute, values, database, input_collection="eventData", result_collection=""):
    
    attribute_filter = [ { "$match": { attribute: { "$in": values}}} ]
    
    return _apply_pipeline(attribute_filter, database, input_collection, result_collection)


# Filters the given collection based on the passed attribute.
# Only events whichs attribute is contained in the passed range are returned.
def event_attribute_in_range(attribute, min, max, database, input_collection="eventData", result_collection=""):
    
    range_filter = [
                    { "$match": { "$and": [
                                          { attribute: { "$gte": min} },
                                          { attribute: { "$lte": max} }
                                          ]
                                 }
                    }
                   ]
                        
    return _apply_pipeline(range_filter, database, input_collection, result_collection)


# Filters the given collection based on the passed timestamp attribute.
# Only events which occured in the given time interval are returned.
def event_in_timerange(time, start, end, database, input_collection="eventData", result_collection=""):
    
    return event_attribute_in_range(time, start, end, database, input_collection, result_collection)


# ------------------------ Case level filters ------------------------


# Filters the given collection based on the passed start activity.
# Only events that belong to cases that have the given start activity are returned.
def case_starts_with(start_activity, database, input_collection="eventData", result_collection=""):
    return _cases_with_event_at(start_activity, ORDER_ASCENDING, database, input_collection, result_collection)


# Filters the given collection based on the passed end activity.
# Only events that belong to cases that have the given end activity are returned.
def case_ends_with(end_activity, database, input_collection="eventData", result_collection=""):
    return _cases_with_event_at(end_activity, ORDER_DESCENDING, database, input_collection, result_collection)


# Filters the given collection based on the performance of the case.
# Only events which belong to a case with a performance in the specified interval are returned.
def case_performance_in_range(min, max, database, input_collection="eventData", result_collection=""):
    
    # The passed time is expected to be in seconds, we are calculating milliseconds though
    milli_min = min * 1000
    milli_max = max * 1000
    
    get_performing_cases = [
                            # order events according to timestamp
                            { "$sort" : { "time:timestamp" : ORDER_ASCENDING}},
                        
                            # group events based on the caseID, keep first event of case
                            { "$group": {
                                "_id": "$case:concept:name",
                                "startTime": { "$first": "$time:timestamp"},
                                "endTime": { "$last": "$time:timestamp"}
                                }
                            },
                      
                            # Calculate the performance of the case
                            { "$project": {
                                "_id": 1,
                                "performance": { "$subtract": [ "$endTime", "$startTime"]}
                                }
                            },
                      
                            # Keep only cases that are within the given performance range
                            { "$match": { "$and": [
                                             { "performance": { "$gte": milli_min }},
                                             { "performance": { "$lte": milli_max }}
                                             ]
                                }
                            }
                            ]
    result = database[input_collection].aggregate(get_performing_cases, allowDiskUse=True)
    
    # Get the ids of cases fulfilling the condition
    cases = [ case["_id"] for case in result]

    return event_attribute_one_of("case:concept:name", cases, database, input_collection, result_collection)


# Filters the given collection based on the passed attribute.
# Only events belonging to cases in which the attribute corresponds to one of the given values are returned.
def case_attribute_one_of(attribute, values, database, input_collection="eventData", result_collection=""):
    
    case_attribute_filter = [
                            # Only keep events with matching attribute
                            { "$match": { attribute: { "$in": values}}},
                            
                            # group events based on the caseID
                            { "$group": {
                                "_id": "$case:concept:name"
                                }
                            }
                            ]
                        
    result = database[input_collection].aggregate(case_attribute_filter, allowDiskUse=True)
    
    # Get the ids of cases fulfilling the condition
    cases = [ case["_id"] for case in result]

    return event_attribute_one_of("case:concept:name", cases, database, input_collection, result_collection)

# Filters the given collection based on the passed time attribute.
# Only cases that intersect or are contained in the given time range are returned.
def case_in_timerange(time, start, end, database, input_collection="eventData", result_collection=""):
    
    # conditions expressing that the case either starts, ends or intersects in the given timeframe
    cond_starts_in_range = { "$and": [
                                      { "startTime": { "$gte": start }},
                                      { "startTime": { "$lte": end }}
                                     ]
                            }
    
    cond_ends_in_range = { "$and": [
                                    { "endTime": { "$gte": start }},
                                    { "endTime": { "$lte": end }}
                                    ]
                         }
    cond_contains_range = { "$and": [
                                      { "startTime": { "$lte": start }},
                                      { "endTime": { "$gte": end }}
                                      ]
                             }
    
    get_cases_in_range = [
                            # order events according to the time attribute
                            { "$sort" : { time : ORDER_ASCENDING}},
                            
                            # group events based on the caseID, keep first event of case
                            { "$group": {
                            "_id": "$case:concept:name",
                            "startTime": { "$first": ("$"+time) },
                            "endTime": { "$last": ("$"+time) }
                            }
                            },

                            # Keep only cases that intersect the given timeframe
                            { "$match": { "$or": [
                                                  cond_contains_range,
                                                  cond_starts_in_range,
                                                  cond_ends_in_range
                                                 ]
                                }
                            }
                            ]
    
    result = database[input_collection].aggregate(get_cases_in_range, allowDiskUse=True)
    
    # Get the ids of cases fulfilling the condition
    cases = [ case["_id"] for case in result]

    return event_attribute_one_of("case:concept:name", cases, database, input_collection, result_collection)

# Returns all variants that are contained in the log and their frequencies
def get_variants(database, include_cases = False, input_collection="eventData", result_collection="variants"):
    
    obtain_variants = [
                       # Sort the events based on their timestamp
                       { "$sort": { "time:timestamp": 1 }},
                             
                       # Group events based on the case id, only keep activity names
                       { "$group": {
                            "_id": "$case:concept:name",
                            "activities": {
                                "$push": { "name": "$concept:name" }
                                }
                            }
                       },
                       
                       # Transform the list of activities into an object that can be used as _id
                       {"$project":
                            {"activities": {
                                "$reduce": {
                                    "input": "$activities",
                                    "initialValue": "",
                                    "in": { "$substrBytes":
                                        [{ "$concat": ["$$value", "->", "$$this.name"]}, 0, 1000]
                                        }
                                    }
                                }
                            }
                       }
                       ]
                       
    # Group events based on the ordered activities
    group_without_cases = [{
                          "$group": {
                          "_id": "$activities",
                          "frequency": { "$sum": 1 }
                          }
                         }]
                       
    # Group events based on the ordered activities and include case identifiers
    group_with_cases = [{
                        "$group": {
                            "_id": "$activities",
                            "frequency": { "$sum": 1 },
                            "cases": { "$push": { "id": "$_id" } }
                        }
                       }]
                       
    # Sort variants based on frequency
    sort_frequency = [ {"$sort": { "frequency": ORDER_DESCENDING }}]
    
    if include_cases:
        obtain_variants += group_with_cases
    else:
        obtain_variants += group_without_cases

    obtain_variants += sort_frequency
                             
    return _apply_pipeline(obtain_variants, database, input_collection, result_collection)

# Returns all variants that have a relative frequency greater then the threshold
# Returns the variant, the frequency and the relative frequency
def get_frequent_variants(database, threshhold, include_cases = False, input_collection="eventData", result_collection="frequentVariants"):

    variants = get_variants(database, input_collection = input_collection)

    # In order to obtain the count of all cases, sum the frequencies of all variants
    count_cases = [{"$group": {
                        "_id": "-",
                        "caseCount": { "$sum": "$frequency"}
                        }
                   }]

    caseCount = variants.aggregate(count_cases, allowDiskUse=True).next()["caseCount"]

    obtain_frequent_variants = [
                                # Compute the relative frequency for all variants
                                { "$addFields": {
                                    "relativeFrequency": { "$divide": ["$frequency", caseCount] }
                                    }
                                },
                                
                                # Return only the variants with a relative frequency higher than the threshold
                                { "$match": { "relativeFrequency": {"$gte": threshhold} } }
                                ]

    return _apply_pipeline(obtain_frequent_variants, database, "variants", result_collection)

# Filters the given collection based on the passed list of variants.
# Only events belonging to cases that correspond to one of the given variants are returned.
def variants(values, database, input_collection="eventData", result_collection=""):

    # Get all variants of the log, include case identifiers
    variants = get_variants(database, include_cases=True, input_collection = input_collection)
    
    identifiers = [("->"+"->".join(variant)) for variant in values]

    # Get the case identifiers of cases that match one of the variants (as objects)
    result = [variant["cases"] for variant in variants.find({ "_id": { "$in": identifiers}})]

    # Get the identifiers
    cases = [case["id"] for cases in result for case in cases]
                             
    return event_attribute_one_of("case:concept:name", cases, database, input_collection, result_collection)


# ------------------------ Internal helper functions ------------------------


# Applies the given pipeline to the input collection and returns the specified output.
def _apply_pipeline(pipeline, database, input_collection, result_collection):
    if result_collection is not "":
        pipeline.append({ "$out" : result_collection })

    result = database[input_collection].aggregate(pipeline, allowDiskUse=True)

    if result_collection is "":
        return list(result)
    else:
        return database[result_collection]

def _cases_with_event_at(target_activity, ordering, database, input_collection, result_collection):
    get_target_event = [
                        # order events according to timestamp
                        { "$sort" : { "time:timestamp" : ordering}},
                        
                        # group events based on the caseID, keep first event of case
                        { "$group": {
                            "_id": "$case:concept:name",
                            "target": { "$first": "$concept:name"}
                            }
                        },
                        
                        # only keep the target activity
                        { "$match": { "target": target_activity}},
                        
                        # group cases by activities
                        { "$group": {
                            "_id": "$target",
                            "cases": { "$push": "$_id"}
                            }
                        }
                        ]
        
    result = list(database[input_collection].aggregate(get_target_event,allowDiskUse=True))
    
    # check if there are any results
    if len(result) == 0:
        cases = []
    else:
        cases = result[0]["cases"]

    # use the attribute filter to return only events that belong to the matching cases
    return event_attribute_one_of("case:concept:name", cases, database, input_collection, result_collection)
