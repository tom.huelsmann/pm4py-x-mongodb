from pymongo import MongoClient
import datetime
import codecs
import csv

# Insert the contents of the given file into an existing database
def insert_from_csv(database, filepath, delimiter=",", batchsize=0, client=MongoClient()):
    return _import_csv(database, filepath, delimiter, batchsize)

# Create a new database containing the contents of the given file
def create_from_csv(name, filepath, delimiter=",", batchsize=0, client=MongoClient()):
    client.drop_database(name)  #deletes an existing database (if it exists already)
    database = client[name] #creates a new database
    collection = database["eventData"] #creates a new collection

    return _import_csv(database, filepath, delimiter, batchsize)

# Helper function used for both import approaches
def _import_csv(database, filepath, delimiter, batchsize):
    print("Importing \""+filepath+"\"...", end="",flush=True)
    
    file = codecs.open(filepath, "r", encoding="utf-8", errors="ignore")
    
    reader = csv.reader(file, delimiter=delimiter, quoting=csv.QUOTE_MINIMAL, quotechar='"')
    header = next(reader)
    
    count = 0
    documents = []
    
    # Iterate over the file row-by-row
    for row in reader:
        document = {}
        count += 1
        index = 0
        for property in header:
            value = row[index]
            
            if property == "time:timestamp":
                casted_value = datetime.datetime.strptime(value[:19], "%Y-%m-%d %H:%M:%S")
            else:
                try:
                    casted_value = float(value)
                except ValueError:
                    casted_value = value
            
            document[property] = casted_value
            index += 1
        documents.append(document)
    
        # If a non-0 batch size is specified, insert the documents when the temporary storage has the size of batchsize
        if batchsize != 0 and count % batchsize == 0:
            print(".",end="",flush=True)
            database.eventData.insert_many(documents)
            documents = []
        
    database.eventData.insert_many(documents)

    file.close()
    print("\nDone!")
    return database
