import pymongo
import collections
from bson.code import Code

AGGREGATION_AVG = "average"
AGGREGATION_MED = "median" #not implemented
AGGREGATION_MAX = "max"
AGGREGATION_MIN = "min"

DFG_FREQUENCY = "frequency"
DFG_PERFORMANCE = "performance"

# Calculate the DFG of the passed collection.
# It is possible to choose the variant and an aggregation type for the performance calculation.
def calculate(database, collection, variant=DFG_FREQUENCY, aggregation=AGGREGATION_AVG, result_collection="dfg"):
    if aggregation == AGGREGATION_MED:
        raise NotImplementedError(aggregation +" is not yet implemented.")
    
    if variant == DFG_FREQUENCY:
        return _dfg_frequency(database, collection, result_collection)
    elif variant == DFG_PERFORMANCE:
        return _dfg_performance(database, collection, aggregation, result_collection)
    else:
        raise ValueError(variant+" is not a vaild variant.")

# Returns a dictionary representation of the DFG stored in the database.
# This only works if the DFG has previously been calculated.
def get_dict(database, input_collection="dfg"):
    dataBaseDFG = database[input_collection].find({})
    
    edges = collections.Counter()
    
    for edge in dataBaseDFG:
        key = edge["_id"]
        start = key["n1"]
        end = key["n2"]
        value = edge["value"]
        
        edges.update({(start, end): value})
    
    if len(edges) == 0:
        raise RuntimeError("It seems like the DFG has not yet been calculated for the given collection. Calculate the DFG using the calculate() function before calling get_dict().")

    return edges

# Helper function that calculates the frequency based DFG for a given collection
def _dfg_frequency(database, collection, result_collection):
    frequency_group_sort = [
                            # Sort the events based on their timestamp
                            { "$sort": { "time:timestamp": 1 }},
                            
                            # Group events based on the case id
                            { "$group": {
                                "_id": "$case:concept:name",
                                "activities": {
                                    "$push": {
                                        "name": "$concept:name"
                                    }
                                }
                            }
                            },
                            
                            # Output the result in a new collection so that map reduce can be applied
                            { "$out": "_casesFreq" }
    ]

    # Apply the pipeline that brings the database content into a suitable format for map reduce
    collection.aggregate(frequency_group_sort, allowDiskUse=True)
    
    map = Code("function() {"
               "    var activities = this['activities'];"
               "    for (i = 0; i < activities.length-1; i++) { "
               "        var node1 = String(activities[i].name);"
               "        var node2 = String(activities[i+1].name);"
               "        emit({ n1: node1, n2: node2}, 1);"
               "    }"
               "}")

    reduce = Code("function(key, counts) {"
                  "     var result = 0;"
                  "     for (var i = 0; i < counts.length; i++) {"
                  "         result += counts[i];"
                  "     }"
                  "     return result;"
                  "}")
    
    # Apply map and reduce functions
    database["_casesFreq"].map_reduce(map = map, reduce = reduce, out = result_collection)

    return database[result_collection]

# Helper function that calculates the performance based DFG for a given collection
def _dfg_performance(database, collection, aggregation, result_collection):
    performance_group_sort = [
                              # Sort the events based on their timestamp
                              { "$sort": { "time:timestamp": 1 }},
                              
                              # Group events based on the case id
                              { "$group": {
                              "_id": "$case:concept:name",
                              "activities" : {
                              "$push": {
                              "name": "$concept:name",
                              "timestamp": "$time:timestamp"
                              }
                              }
                              }
                              },
                              
                              # Output the result in a new collection so that map reduce can be applied
                              { "$out" : "_casesPerf" }
                            ]
                            
    # Apply the pipeline that brings the database content into a suitable format for map reduce
    collection.aggregate(performance_group_sort, allowDiskUse = True)
    
    performance_map = Code("function() {"
                "    var activities = this['activities'];"
                "    for (i = 0; i < activities.length-1; i++) { "
                "        var first = activities[i];"
                "        var second = activities[i+1];"
                "        var duration = (second.timestamp - first.timestamp) / 1000;"
                "        emit({ n1: String(first.name), n2: String(second.name)}, {'duration': duration, 'count': 1});"
                "    }"
                "}")
    
    performance_reduce = Code("function(key, values) {"
                "     var result = {'duration':0, 'count':0};"
                "     for (var i = 0; i < values.length; i++) {"
                "         result.duration += values[i].duration;"
                "         result.count += values[i].count;"
                "     }"
                "     return result;"
                "}")
                  
    performance_avg_finalize = Code("function(key, value) {"
                    "   return value.duration / value.count;"
                    "}")
                    
    performance_map_min_max = Code("function() {"
                           "    var activities = this['activities'];"
                           "    for (i = 0; i < activities.length-1; i++) { "
                           "        var first = activities[i];"
                           "        var second = activities[i+1];"
                           "        var duration = (second.timestamp - first.timestamp) / 1000;"
                           "        emit({ n1: String(first.name), n2: String(second.name)}, duration);"
                           "    }"
                           "}")
    
    performance_reduce_max = Code("function(key, values) {"
                                  "     return Math.max(...values);"
                                  "}")
                                  
    performance_reduce_min = Code("function(key, values) {"
                                  "     return Math.min(...values);"
                                  "}")
    
    # Apply the suitable pipeline for the given aggregation variant
    if aggregation == AGGREGATION_AVG:
        database["_casesPerf"].map_reduce(map = performance_map, reduce = performance_reduce, out = result_collection, finalize = performance_avg_finalize)
    
    elif aggregation == AGGREGATION_MAX:
        database["_casesPerf"].map_reduce(map = performance_map_min_max, reduce = performance_reduce_max, out = result_collection)

    elif aggregation == AGGREGATION_MIN:
        database["_casesPerf"].map_reduce(map = performance_map_min_max, reduce = performance_reduce_min, out = result_collection)

    else:
        raise ValueError(aggregation+" is not a vaild aggregation type.")
                            
    return database[result_collection]

