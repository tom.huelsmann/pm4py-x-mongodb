#import pprint
#import pandas as pd
#import time
#from pymongo import MongoClient
#from pm4py.objects.log.importer.csv import factory as csv_import_factory
#import importer
##
##
##def newDataBaseFromCSVUsingPandas(name, path):
##    data = pd.read_csv(path)
##
##    dataBase = client[name]    #create a new dataBase with the given name
##    collection = dataBase.eventData
##    collection.drop()
##    collection = dataBase.eventData
##
##    for index, row in data.iterrows():
##        columnIndex = 0
##        columns = list(data)
##        document = {}
##
##        for columnValue in row:
##            columnName = columns[columnIndex]
##            document[columnName] = columnValue
##            columnIndex += 1
##
##        collection.insert_one(document)
##
##    return dataBase
##
##def newDataBaseFromCSV(name, path):
##    file = open(path, "r")
##
##    dataBase = client[name]    #create a new dataBase with the given name
##    collection = dataBase.eventData
##    collection.drop()
##    collection = dataBase.eventData
##
##    header = file.readline().split(",")
##
##    for row in file:
##        document = {}
##        index = 0
##        for property in header:
##            document[property] = row.split(",")[index]
##            index += 1
##        collection.insert_one(document)
##
##    return dataBase
##
##def newDataBaseFromCSVBatch(name, path):
##    file = open(path, "r")
##
##    dataBase = client[name]    #create a new dataBase with the given name
##    collection = dataBase.eventData
##    collection.drop()
##    collection = dataBase.eventData
##
##    header = file.readline().split(",")
##    documents = []
##    for row in file:
##        document = {}
##        index = 0
##        for property in header:
##            document[property] = row.split(",")[index]
##            index += 1
##        documents.append(document)
##    collection.insert_many(documents)
##
##    return dataBase
##
##def calculateDFG(collection):
##    #{ "$group" : { "_id" : "$case:concept:name"}}
##    groupPipeline = [
##                     { "$group" : {
##                        "_id" : "$case:concept:name",
##                        "data" : {
##                            "$push" : { "activity" : "$concept:name",
##                                        "timestamp" : "$time:timestamp"
##                                    }
##                                }
##                            }
##                     },
##                     {"$sort" : { "timestamp" : -1}}
##                    ]
##
##    grouped = collection.aggregate(groupPipeline)
##
##    pprint.pprint(list(grouped))
##
##start = time.time()
##dataBase = csv_import.newDataBaseFromCSVBatch("running", "receipt.csv")
##end = time.time()
##print(end-start)
##
#
##show contents of db
##result = dataBase.eventData.find({"case:concept:name": "case-10025"})
##pprint.pprint(list(result))
#
## dfg calculation
##calculateDFG(dataBase.eventData)
#
#client = MongoClient()
#database = client["eventData"]
#
#database= importer.insert_from_csv(database, "running-example.csv")
#print(len(list(database.eventData.find({}))))
#
#

from pm4py.visualization.dfg import factory as dfg_vis_factory

from source import importer
from source import filter
from source import dfg

# Import a CSV file
database = importer.create_from_csv("dataset", "evaluation/receipt.csv", batchsize=5000)

# Filter the log based on the end activity
filtered_end = filter.case_ends_with("Confirmation of receipt", database, result_collection="filteredEnd")

# Calculate the performance based dfg and get the edges as a dictionary
dfg.calculate(database, filtered_end, variant="performance")
edges = dfg.get_dict(database, input_collection="dfg")

# Visualize the dfg using PM4Py
visualization = dfg_vis_factory.apply(edges, variant="performance")
dfg_vis_factory.view(visualization)
