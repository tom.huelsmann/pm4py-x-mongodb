from pymongo import MongoClient
import datetime
import pandas as pd
import pprint
import codecs
import csv

#import the CSV using the pandas csv importer
def newDatabaseFromCSVUsingPandas(name, path):
    client = MongoClient() #Using the default host and port
    data = pd.read_csv(path)
    
    database = client[name]    #create a new dataBase with the given name
    collection = database.eventData
    collection.drop()
    collection = database.eventData
    
    for index, row in data.iterrows():
        columnIndex = 0
        columns = list(data)
        document = {}
        
        for columnValue in row:
            columnName = columns[columnIndex]
            document[columnName] = columnValue
            columnIndex += 1
        
        collection.insert_one(document)
    
    return database

#import the CSV using the pandas csv importer and add them to the database as a batch
def newDatabaseFromCSVUsingPandasBatch(name, path):
    client = MongoClient() #Using the default host and port
    data = pd.read_csv(path)
    
    database = client[name]    #create a new dataBase with the given name
    collection = database.eventData
    collection.drop()
    collection = database.eventData
    documents = []
    
    for index, row in data.iterrows():
        columnIndex = 0
        columns = list(data)
        document = {}
        
        for columnValue in row:
            columnName = columns[columnIndex]
            document[columnName] = columnValue
            columnIndex += 1

        documents.append(document)
    collection.insert_many(documents)

    return database

#manually read the csv
def newDatabaseFromCSV(name, path):
    client = MongoClient() #Using the default host and port
    file = open(path, "r")
    
    database = client[name]    #create a new dataBase with the given name
    collection = database.eventData
    collection.drop()
    collection = database.eventData
    
    header = file.readline().split(",")
    
    for row in file:
        document = {}
        index = 0
        for property in header:
            document[property] = row.split(",")[index]
            index += 1
        collection.insert_one(document)
    
    file.close()
    return database

#manually read the csv and add the documents as a batch
# this is the chosen one, the other methods should be removed later
def newDatabaseFromCSVBatch(name, path, delimiter=","):
    client = MongoClient() #Using the default host and port
    
    #stuff for creating and overriding databases and collections
    database = client[name]    #create a new dataBase with the given name
    collection = database.eventData
    collection.drop()
    collection = database.eventData
    
    #actual import
    file = open(path, "r")
    documents = []
    header = _row_to_list(file.readline(), delimiter) #CAKE: check if there is column with empty name
    
    for row in file:
        document = {}
        index = 0
        for property in header:
            document[property] = _row_to_list(row, delimiter)[index]
            index += 1
        documents.append(document)

    collection.insert_many(documents)

    file.close()
    return database

#read the csv using the python csv reader and add the documents one by one

def newDatabaseFromCSVStandard(name, path, delimiter=","):
    client = MongoClient() #Using the default host and port
    
    #stuff for creating and overriding databases and collections
    database = client[name]    #create a new dataBase with the given name
    collection = database.eventData
    collection.drop()
    collection = database.eventData
    
    #actual import
    file = open(path, "r")
    reader = csv.reader(file, delimiter=delimiter)
    header = next(reader)
    
    for row in reader:
        document = {}
        index = 0
        for property in header:
            document[property] = row[index]
            index += 1
        collection.insert_one(document)

    file.close()
    return database

def _row_to_list(row, delimiter=","):
    return row.rstrip().split(delimiter)

#-----------------------------------------------------------
#everything above this line is just implementation testing stuff, the real functions that should be used later are located here.

def insert_from_csv(database, filepath, delimiter=",", batchsize=0, client=MongoClient()):
    return _import_csv(database, filepath, delimiter, batchsize)

def create_from_csv(name, filepath, delimiter=",", batchsize=0, client=MongoClient()):
    client.drop_database(name)  #deletes an existing database (if it exists already)
    database = client[name] #creates a new database
    collection = database["eventData"] #creates a new collection

    return _import_csv(database, filepath, delimiter, batchsize)

def _import_csv(database, filepath, delimiter, batchsize):
    print("Importing \""+filepath+"\"...", end="",flush=True)
    
    file = codecs.open(filepath, "r", encoding="utf-8", errors="ignore")
    
    reader = csv.reader(file, delimiter=delimiter, quoting=csv.QUOTE_MINIMAL, quotechar='"')
    header = next(reader)
    
    count = 0
    documents = []
    
    # Iterate over the file
    for row in reader:
        document = {}
        count += 1
        index = 0
        for property in header:
            value = row[index]
            
            if property == "time:timestamp":
                casted_value = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S") #CAKE change to default iso
            else:
                try:
                    casted_value = float(value)
                except ValueError:
                    casted_value = value
            
            document[property] = casted_value
            index += 1
        documents.append(document)
    
        if batchsize != 0 and count % batchsize == 0:
            print(".",end="",flush=True)
            database.eventData.insert_many(documents)
            documents = []
        
    database.eventData.insert_many(documents)

    file.close()
    print("\nDone!")
    return database
