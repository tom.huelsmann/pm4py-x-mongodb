import importer
import dfg
import filter
import pprint
import time

from pm4py.objects.log.importer.csv import factory as csv_import_factory

import os
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.algo.discovery.dfg import factory as dfg_factory
from pm4py.visualization.dfg import factory as dfg_vis_factory
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.objects.log.util import sorting
from pm4py.algo.filtering.log.end_activities import end_activities_filter
from pm4py.algo.filtering.log.cases import case_filter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.algo.filtering.log.variants import variants_filter
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.algo.discovery.dfg.adapters.pandas import df_statistics
from pm4py.util import constants

filepath = "bpi2019_cleaned.csv"

#from pymongo import MongoClient
#client = MongoClient()
#
#start_mongo = time.time()
#start_mongo_import = time.time()
#
#database = importer.create_from_csv("bpi2019", filepath, batchsize=50000)
#
#end_mongo_import = time.time()
#
#start_mongo_dfg = time.time()
#
#dfg.calculate(database, database.eventData, variant="frequency")
#
#edges = dfg.get_dict(database)
#
#end_mongo_dfg = time.time()
#
#visualization = dfg_vis_factory.apply(edges, variant="frequency")
#dfg_vis_factory.view(visualization)
#
#net, initial_marking, final_marking = inductive_miner.apply_dfg(edges)
#visualization_net = pn_vis_factory.apply(net, initial_marking, final_marking)
#pn_vis_factory.view(visualization_net)
#
#end_mongo = time.time()
#
#print("MongoDB stats:")
#print("Import: "+str(end_mongo_import-start_mongo_import)+"s")
#print("DFG Calculation: "+str(end_mongo_dfg-start_mongo_dfg)+"s\n")
#print("Overall: "+str(end_mongo-start_mongo)+"s")
#print("--------------------------------------------")

# ---------------------------- PM4Py part ----------------------------

start_pm4py = time.time()
start_pm4py_import = time.time()
#
#CASEID_GLUE = "case:concept:name"
#ACTIVITY_KEY = "concept:name"
#TIMEST_KEY = "time:timestamp"
#TIMEST_COLLUMNS = ["time:timestamp"]
#TIMEST_FORMAT = "%Y-%m-%d %H:%M:%S"
#
#parameters = { constants.PARAMETER_CONSTANT_CASEID_KEY: CASEID_GLUE, constants.PARAMETER_CONSTANT_ACTIVITY_KEY:ACTIVITY_KEY}

dataframe = csv_import_adapter.import_dataframe_from_path(filepath)

end_pm4py_import = time.time()

start_pm4py_dfg = time.time()

dfg_pm4py = df_statistics.get_dfg_graph(dataframe, measure="performance")

end_pm4py_dfg = time.time()

visualization_pm4py = dfg_vis_factory.apply(dfg_pm4py, variant="frequency")

net_pm4py, initial_marking_pm4py, final_marking_pm4py = inductive_miner.apply_dfg(dfg_pm4py)
visualization_net_pm4py = pn_vis_factory.apply(net_pm4py, initial_marking_pm4py, final_marking_pm4py)
pn_vis_factory.view(visualization_net_pm4py)

end_pm4py = time.time()

print("PM4Py stats:")
print("Import: "+str(end_pm4py_import-start_pm4py_import)+"s")
print("DFG Calculation: "+str(end_pm4py_dfg-start_pm4py_dfg)+"s\n")
print("Overall: "+str(end_pm4py-start_pm4py)+"s")
print("--------------------------------------------")




