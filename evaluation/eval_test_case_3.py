import importer
import dfg
import filter
import pprint
import time

from pm4py.objects.log.importer.csv import factory as csv_import_factory

import os
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.algo.discovery.dfg import factory as dfg_factory
from pm4py.visualization.dfg import factory as dfg_vis_factory
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.util import constants
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.algo.filtering.pandas.attributes import attributes_filter

filepath = "roadtraffic_cleaned.csv"

#from pymongo import MongoClient
#client = MongoClient()
#
#start_mongo = time.time()
#start_mongo_import = time.time()
#
#database = importer.create_from_csv("roadtraffic", filepath, batchsize=50000)
#
#end_mongo_import = time.time()
#
#start_mongo_attr = time.time()
#
#filtered_attr = filter.case_attribute_one_of("org:resource", [561, 550], database, result_collection="filteredResource")
#
#end_mongo_attr = time.time()
#
#start_mongo_dfg = time.time()
#
#dfg.calculate(database, filtered_attr, variant="performance")
#
#edges = dfg.get_dict(database)
#
#end_mongo_dfg = time.time()
#
#visualization = dfg_vis_factory.apply(edges, variant="performance")
#dfg_vis_factory.view(visualization)
#
#net, initial_marking, final_marking = inductive_miner.apply_dfg(edges)
#visualization_net = pn_vis_factory.apply(net, initial_marking, final_marking)
#pn_vis_factory.view(visualization_net)
#
#end_mongo = time.time()
#
#print("MongoDB stats:")
#print("Import: "+str(end_mongo_import-start_mongo_import)+"s")
#print("Filter attribute: "+str(end_mongo_attr-start_mongo_attr)+"s")
#print("DFG Calculation: "+str(end_mongo_dfg-start_mongo_dfg)+"s\n")
#print("Overall: "+str(end_mongo-start_mongo)+"s")
#print("--------------------------------------------")

# ---------------------------- PM4Py part ----------------------------

start_pm4py = time.time()
start_pm4py_import = time.time()

dataframe = csv_import_adapter.import_dataframe_from_path(filepath)

end_pm4py_import = time.time()

start_pm4py_attr = time.time()

filtered_log_attr = attributes_filter.apply(dataframe, [561, 550], parameters={constants.PARAMETER_CONSTANT_ATTRIBUTE_KEY: "org:resource"})

end_pm4py_attr = time.time()

start_pm4py_dfg = time.time()

dfg_pm4py = dfg_factory.apply(filtered_log_attr, variant="performance")

end_pm4py_dfg = time.time()

visualization_pm4py = dfg_vis_factory.apply(dfg_pm4py, log=filtered_log_attr, variant="performance")

net_pm4py, initial_marking_pm4py, final_marking_pm4py = inductive_miner.apply_dfg(dfg_pm4py)
visualization_net_pm4py = pn_vis_factory.apply(net_pm4py, initial_marking_pm4py, final_marking_pm4py)
pn_vis_factory.view(visualization_net_pm4py)

end_pm4py = time.time()

print("PM4Py stats:")
print("Import: "+str(end_pm4py_import-start_pm4py_import)+"s")
print("Filter attribute: "+str(end_pm4py_attr-start_pm4py_attr)+"s")
print("DFG Calculation: "+str(end_pm4py_dfg-start_pm4py_dfg)+"s\n")
print("Overall: "+str(end_pm4py-start_pm4py)+"s")
print("--------------------------------------------")




