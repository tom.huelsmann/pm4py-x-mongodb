import importer
import dfg
import filter
import pprint
import time

from pm4py.objects.log.importer.csv import factory as csv_import_factory

import os
from pm4py.objects.log.importer.csv import factory as csv_importer
from pm4py.algo.discovery.dfg.adapters.pandas import df_statistics
from pm4py.visualization.dfg import factory as dfg_vis_factory
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory
from pm4py.objects.log.util import sorting
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.algo.filtering.pandas.end_activities import end_activities_filter
from pm4py.algo.filtering.pandas.cases import case_filter
from pm4py.algo.filtering.pandas.variants import variants_filter

filepath = "bpi2019_cleaned.csv"

from pymongo import MongoClient
client = MongoClient()

start_mongo = time.time()
start_mongo_import = time.time()

database = importer.create_from_csv("bpi2019", filepath, batchsize=50000)

end_mongo_import = time.time()

start_mongo_end = time.time()

filtered_end = filter.case_ends_with("Clear Invoice", database, result_collection="filteredEnd")
print("End act: "+str(filtered_end.count({})))

end_mongo_end = time.time()

start_mongo_perf = time.time()

filtered_performance = filter.case_performance_in_range(0, 60*60*24*31, database, input_collection="filteredEnd", result_collection="filteredPerf")
print("Perf: "+str(filtered_performance.count({})))

end_mongo_perf = time.time()

start_mongo_var = time.time()

variants_coll = filter.get_frequent_variants(database, 0.05, input_collection ="filteredPerf", result_collection="")

variants = [string.split("->")[1:] for string in [variant["_id"] for variant in variants_coll]]

filtered_variants = filter.variants(variants, database, input_collection ="filteredPerf", result_collection="filteredLog")
print("Var: "+str(filtered_variants.count({})))

end_mongo_var = time.time()

start_mongo_dfg = time.time()

dfg.calculate(database, filtered_variants, variant="performance")

edges = dfg.get_dict(database)

end_mongo_dfg = time.time()

visualization = dfg_vis_factory.apply(edges, variant="performance")
dfg_vis_factory.view(visualization)

net, initial_marking, final_marking = inductive_miner.apply_dfg(edges)
visualization_net = pn_vis_factory.apply(net, initial_marking, final_marking)
pn_vis_factory.view(visualization_net)

end_mongo = time.time()

print("MongoDB stats:")
print("Import: "+str(end_mongo_import-start_mongo_import)+"s")
print("Filter end activity: "+str(end_mongo_end-start_mongo_end)+"s")
print("Filter performance activity: "+str(end_mongo_perf-start_mongo_perf)+"s")
print("Filter frequent variants: "+str(end_mongo_var-start_mongo_var)+"s")
print("DFG Calculation: "+str(end_mongo_dfg-start_mongo_dfg)+"s\n")
print("Overall: "+str(end_mongo-start_mongo)+"s")
print("--------------------------------------------")

# ---------------------------- PM4Py part ----------------------------

#start_pm4py = time.time()
#start_pm4py_import = time.time()
#
#parameters = {}
#parameters["case_id_glue"] = "case:concept:name"
#parameters["pm4py:param:activity_key"] = "concept:name"
#parameters["pm4py:param:timestamp_key"] = "time:timestamp"
#parameters["timest_columns"] = ["time:timestamp"]
#parameters["timest_format"] = "%Y-%m-%d %H:%M:%S"
#
#dataframe = csv_import_adapter.import_dataframe_from_path(filepath)
#
#end_pm4py_import = time.time()
#
#start_pm4py_end = time.time()
#
#filtered_log_end = end_activities_filter.apply(dataframe, ["Clear Invoice"])
#
#end_pm4py_end = time.time()
#
#start_pm4py_perf = time.time()
#
#filtered_log_performance = case_filter.filter_on_case_performance(filtered_log_end, min_case_performance=0, max_case_performance=60*60*24*31)
#
#end_pm4py_perf = time.time()
#
#start_pm4py_var = time.time()
#
#filtered_log_variants = variants_filter.apply_auto_filter(filtered_log_performance, parameters={"decreasingFactor":0.31})
#
#end_pm4py_var = time.time()
#
#start_pm4py_dfg = time.time()
#
#dfg_pm4py = df_statistics.get_dfg_graph(filtered_log_variants, measure="performance")
#
#end_pm4py_dfg = time.time()
#
#visualization_pm4py = dfg_vis_factory.apply(dfg_pm4py, variant="performance")
#dfg_vis_factory.view(visualization_pm4py)
#
#net_pm4py, initial_marking_pm4py, final_marking_pm4py = inductive_miner.apply_dfg(dfg_pm4py)
#visualization_net_pm4py = pn_vis_factory.apply(net_pm4py, initial_marking_pm4py, final_marking_pm4py)
#pn_vis_factory.view(visualization_net_pm4py)
#
#end_pm4py = time.time()
#
#print("PM4Py stats:")
#print("Import: "+str(end_pm4py_import-start_pm4py_import)+"s")
#print("Filter end activity: "+str(end_pm4py_end-start_pm4py_end)+"s")
#print("Filter performance activity: "+str(end_pm4py_perf-start_pm4py_perf)+"s")
#print("Filter frequent variants: "+str(end_pm4py_var-start_pm4py_var)+"s")
#print("DFG Calculation: "+str(end_pm4py_dfg-start_pm4py_dfg)+"s\n")
#print("Overall: "+str(end_pm4py-start_pm4py)+"s")
#print("--------------------------------------------")
#
#
#
#
