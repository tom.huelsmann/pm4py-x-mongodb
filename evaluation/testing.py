import importer
import dfg as dfg
import filter
import pprint
import time
from pm4py.objects.log.importer.csv import factory as csv_import_factory
from pymongo import MongoClient
import os
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4py.algo.discovery.dfg import factory as dfg_factory
from pm4py.visualization.dfg import factory as dfg_vis_factory
from pm4py.algo.discovery.inductive import factory as inductive_miner
from pm4py.visualization.petrinet import factory as pn_vis_factory
import collections
import datetime

#start = time.time()
#dataBase = newDataBaseFromCSV("running", "receipt.csv")
#end = time.time()
#print(end-start)

databaseName = "bpi2019"
filePath = "bpi2019.csv"

def testImport():
    start = time.time()
    database = importer.newDatabaseFromCSVUsingPandas(databaseName, filePath)
    end = time.time()
    print("Pandas: "+str(end-start))

    start = time.time()
    database = importer.newDatabaseFromCSVUsingPandasBatch(databaseName, filePath)
    end = time.time()
    print("Pandas Batch: "+str(end-start))

    start = time.time()
    database = importer.newDatabaseFromCSV(databaseName, filePath)
    end = time.time()
    print("Custom: "+str(end-start))

    start = time.time()
    database = importer.newDatabaseFromCSVBatch(databaseName, filePath)
    end = time.time()
    print("Custom Batch: "+str(end-start))

def testDFG(do_import):
    client = MongoClient()
    if do_import:
        start = time.time()
        
        database = importer.create_from_csv(databaseName, filePath, batchsize=20001)
        end = time.time()
        print("Import: "+str(end-start))
    else:
        database = client.bpi2019
    
    start = time.time()
    dfgCollection = dfg.calculate(database, database.eventData, variant="performance")
    end = time.time()
    edges = dfg.get_dict(database)
    print("DFG calculation: "+str(end-start)+"s")

#
#    start = time.time()
#log = xes_importer.import_log(os.path.join("Offer.xes"))
#    end = time.time()
#    print("import PM4Py: "+str(end-start)+"s")
#    start = time.time()
#    dfgPM4Py = dfg_factory.apply(log, variant="performance")
#    gviz = dfg_vis_factory.apply(dfgPM4Py, log=log, variant="performance")
#    end = time.time()
#    print("DFG calculation PM4Py: "+str(end-start)+"s")

#    print(dfgPM4Py)
    gviz2 = dfg_vis_factory.apply(edges, variant="performance")
#   dfg_vis_factory.view(gviz)
    dfg_vis_factory.view(gviz2)

    net, initial_marking, final_marking = inductive_miner.apply_dfg(edges)
    gvizNet = pn_vis_factory.apply(net, initial_marking, final_marking)
    pn_vis_factory.view(gvizNet)

def testFilter(do_import):
    client = MongoClient()
    if do_import:
        start = time.time()
        
        database = importer.create_from_csv(databaseName, filePath)
        end = time.time()
        print("Import: "+str(end-start))
    else:
        database = client.bpi2019

#    values = ["User_116", "User_1"]
#
#    start = time.time()
#    result = filter.attribute_one_of("org:resource", values, database)
#    end = time.time()
#    print("Filter application: "+str(end-start)+"s")
#
#    list_result = list(result)
#    print(len(list_result))
    #pprint.pprint(list_result[:3])

#result = filter.attribute_one_of("org:resource", values, database)
#    startDate = datetime.datetime(2016, 5, 1, 0, 0, 0)
#    endDate = datetime.datetime(2016, 5, 30, 23, 59, 59)
    start = time.time()
    result = filter.case_ends_with("Clear Invoice", database, result_collection="filtered")
    end  = time.time()
    print(end-start)

#    start = time.time()
#    result = filter.get_frequent_variants(database, threshhold=0.1, result_collection="frequentVariants")
#    end  = time.time()
#    print(end-start)
#    result_list = list(result.find({}))
#    pprint.pprint(len(result_list))
#    pprint.pprint(result_list[:3])

testFilter(False)
#testDFG(True)
#testImport()

#show contents of db
#result = dataBase.eventData.find({"case:department": "Customer contact"})
#pprint.pprint(list(result))
