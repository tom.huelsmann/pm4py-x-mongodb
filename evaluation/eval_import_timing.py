import importer
import pprint
import time
from pm4py.objects.log.importer.csv import factory as csv_import_factory
import resource

#start = time.time()
#dataBase = newDataBaseFromCSV("running", "receipt.csv")
#end = time.time()
#print(end-start)

dataBaseName = "offer"
filePath = "offer.csv"
number_of_runs = 1
#
#sum = 0.0
#min = 10000000.0
#max = 0.0
#for i in range(0,number_of_runs):
#    start = time.time()
#    dataBase = importer.newDatabaseFromCSVUsingPandas(dataBaseName, filePath)
#    end = time.time()
#    delta = (end-start)
#    sum += delta
#    if delta < min:
#        min = delta
#    elif delta > max:
#        max = delta
#    print("Pandas: "+str(end-start))
#print("---------------\n")
#print("Highest: "+str(max))
#print("Lowest: "+str(min))
#print("Average: "+str(sum/(number_of_runs*1.0)))
#print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
#print("---------------\n")

sum = 0.0
min = 10000000.0
max = 0.0
for i in range(0,number_of_runs):
    start = time.time()
    dataBase = importer.newDatabaseFromCSVUsingPandasBatch(dataBaseName, filePath)
    end = time.time()
    delta = (end-start)
    sum += delta
    if delta < min:
        min = delta
    elif delta > max:
        max = delta
    print("Pandas Batch: "+str(end-start))
print("---------------\n")
print("Highest: "+str(max))
print("Lowest: "+str(min))
print("Average: "+str(sum/(number_of_runs*1.0)))
print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
print("---------------\n")

#sum = 0.0
#min = 10000000.0
#max = 0.0
#for i in range(0,number_of_runs):
#    start = time.time()
#    dataBase = importer.newDatabaseFromCSV(dataBaseName, filePath)
#    end = time.time()
#    delta = (end-start)
#    sum += delta
#    if delta < min:
#        min = delta
#    elif delta > max:
#        max = delta
#    print("Custom: "+str(end-start))
#print("---------------\n")
#print("Highest: "+str(max))
#print("Lowest: "+str(min))
#print("Average: "+str(sum/(number_of_runs*1.0)))
#print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
#print("---------------\n")
#
#sum = 0.0
#min = 10000000.0
#max = 0.0
#for i in range(0,number_of_runs):
#    start = time.time()
#    dataBase = importer.newDatabaseFromCSVBatch(dataBaseName, filePath)
#    end = time.time()
#    delta = (end-start)
#    sum += delta
#    if delta < min:
#        min = delta
#    elif delta > max:
#        max = delta
#    print("Custom Batch: "+str(end-start))
#print("---------------\n")
#print("Highest: "+str(max))
#print("Lowest: "+str(min))
#print("Average: "+str(sum/(number_of_runs*1.0)))
#print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
#print("---------------\n")
#
#sum = 0.0
#min = 10000000.0
#max = 0.0
#for i in range(0,number_of_runs):
#    start = time.time()
#    dataBase = importer.newDatabaseFromCSVStandard(dataBaseName, filePath)
#    end = time.time()
#    delta = (end-start)
#    sum += delta
#    if delta < min:
#        min = delta
#    elif delta > max:
#        max = delta
#    print("CSV Reader: "+str(end-start))
#print("---------------\n")
#print("Highest: "+str(max))
#print("Lowest: "+str(min))
#print("Average: "+str(sum/(number_of_runs*1.0)))
#print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
#print("---------------\n")
#
#sum = 0.0
#min = 10000000.0
#max = 0.0
#for i in range(0,number_of_runs):
#    start = time.time()
#    dataBase = importer.newDatabaseFromCSVStandardBatch(dataBaseName, filePath)
#    end = time.time()
#    delta = (end-start)
#    sum += delta
#    if delta < min:
#        min = delta
#    elif delta > max:
#        max = delta
#    print("CSV Reader Batch: "+str(end-start))
#print("---------------\n")
#print("Highest: "+str(max))
#print("Lowest: "+str(min))
#print("Average: "+str(sum/(number_of_runs*1.0)))
#print("memory:"+str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/8000000))
#print("---------------\n")
#
